// import setTestState from './setTestState'
import { useDispatch } from '../context';
import { SET_TEST_STATE } from './types';

const useActionsDispatch = () => {
  const dispatch = useDispatch();

  const setTestState = (newState) => {
    dispatch({ type: SET_TEST_STATE, params: { newState } });
  };

  return {
    setTestState,
  };
};

export default useActionsDispatch;
