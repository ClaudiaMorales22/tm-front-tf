import { Box, Flex, Text } from '@chakra-ui/react';
import '../../App.css';

const SentMessage = () => {
  return (
    <Flex
      alignItems="end"
      direction="column"
      maxW="md"
    >
      <Box
        mt="0.75rem"
        backgroundColor="#7c4188"
        borderRadius="0.562rem"
        alignItems="end"
        className={'sentMessage'}
      >
        <Text
          color="#ffffff"
          fontSize="0.875rem"
          fontWeight="400"
          m="1rem"
        >
          Mensaje de usuarie
        </Text>
      </Box>
    </Flex>
  );
};

export default SentMessage;
