import { Container, Flex, Input } from '@chakra-ui/react';

import TMImage from '../atoms/TMImage';
import TMButton from '../atoms/TMButton';

const UsuarieChatBottom = () => {
  return (
    <Container
      backgroundColor="#ffffff"
      boxShadow="2xl"
    >
      <Flex
        alignItems="center"
        justifyContent="center"
        h="5.188rem"
      >
        <TMButton
          mr={'1rem'}
          variant={'ghost'}
        >
          <TMImage
            src={'/icons/emoji-keyboard.svg'}
            boxSize={'2rem'}
          />
        </TMButton>
        <Input />
        <TMButton
          mr={'1rem'}
          ml={'1rem'}
          variant={'ghost'}
          backgroundColor={'#5C186A'}
          borderRadius={'50%'}
        >
          <TMImage src={'/icons/send.svg'} />
        </TMButton>
      </Flex>
    </Container>
  );
};

export default UsuarieChatBottom;
