import { Button, Container, Flex, Text } from '@chakra-ui/react';
import { useNavigate } from 'react-router-dom';

import TMImage from '../atoms/TMImage';

const WelcomeTemplate = () => {
  const navigate = useNavigate();
  const redirectToAuth = () => navigate('/voluntaries/auth');

  return (
    <Flex
      direction="column"
      margin="146px"
    >
      <Container centerContent>
        <TMImage
          src={'/images/logo-todo-mejora.svg'}
          alt={'Heart'}
          h={'4.25rem'}
          w={'15rem'}
        />
      </Container>
      <Container
        mt="37px"
        w="450px"
      >
        <Text
          fontSize="32px"
          ml="12"
        >
          Bienvenide Voluntarie
        </Text>
        <Text
          fontSize="16px"
          w="391px"
          mt="21px"
          ml="12"
        >
          Con nuestro Programa Hora Segura, línea de ayuda gratuita y
          confidencial, por chat atendemos a miles de niñes, niñas, niños,
          adolescentes y a su entorno protector.
        </Text>
      </Container>
      <Container
        mt="-49px"
        centerContent
        w="417px"
      >
        <TMImage
          src={'/images/family-support.svg'}
          alt={'Family support'}
          w={'417px'}
          h={'345px'}
        />
      </Container>
      <Container
        alignContent="center"
        textAlign="center"
        mt="37px"
      >
        <Button
          bgColor="#161616"
          textColor="#FFFFFF"
          fontSize="14px"
          w="324px"
          h="54px"
          onClick={redirectToAuth}
        >
          Entrar
        </Button>
      </Container>
    </Flex>
  );
};

export default WelcomeTemplate;
