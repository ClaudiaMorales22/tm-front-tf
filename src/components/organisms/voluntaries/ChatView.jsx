import { Box, Text } from '@chakra-ui/react';
import TMImage from '../../atoms/TMImage';

const ChatView = () => {
  return (
    <Box align="center">
      <TMImage
        src={'/images/family-support.svg'}
        w={'26.063rem'}
        h={'21.563rem'}
      />
      <Text
        fontWeight="600"
        fontSize="1.25rem"
        color="#8E8E8E"
        textAlign="left"
      >
        Selecciona un chat y comienza la conversación con nuestros usuaries.
      </Text>
    </Box>
  );
};

export default ChatView;
