import {
  Button,
  ButtonGroup,
  Checkbox,
  Container,
  Flex,
  Image,
  Input,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalOverlay,
  Text,
  useControllableState,
} from '@chakra-ui/react';
import { useNavigate } from 'react-router-dom';

import { buttonProps, buttonPropsClicked } from '../../themes/todomejora';
import TMImage from '../../components/atoms/TMImage';
import TMLinkButton from '../../components/atoms/TMLinkButton';
import TMButton from '../../components/atoms/TMButton';

const UsuarieForm = () => {
  const navigate = useNavigate();
  const handleClick = () => navigate('/usuaries/chat');

  const [acceptedTerms, setAcceptedTerms] = useControllableState(false);

  const [name, setName] = useControllableState('');
  const [age, setAge] = useControllableState('');
  const [comuna, setComuna] = useControllableState('');

  const [gender, setGender] = useControllableState('');
  const [bioSex, setBioSex] = useControllableState('');
  const [sexualOrientation, setSexualOrientation] = useControllableState('');

  const [openModal, setModal] = useControllableState(false);
  const [modalTitle, setModalTitle] = useControllableState('');
  const [modalDescription, setModalDescription] = useControllableState('');

  const clickedProps = (hook, value) => {
    if (hook === value) {
      return buttonPropsClicked;
    } else {
      return buttonProps;
    }
  };

  const validateFields = () => {
    return !!acceptedTerms;
  };

  const showModal = (e) => {
    if (e.target.name === 'identidad-genero') {
      setModalTitle('Identidad de género:');
      setModalDescription(
        'Se refiere a la vivencia interna e individual del género, tal como cada persona la siente profundamente, la cual podría corresponder o no con el sexo asignado al nacer, incluyendo la vivencia personal del cuerpo.'
      );
    }

    if (e.target.name === 'sexo-biologico') {
      setModalTitle('Sexo biológico:');
      setModalDescription(
        'Características biológicas (genéticas, endocrinas y anatómicas) empleadas para otorgarle a una persona la definición de macho, hembra, intersexual.'
      );
    }

    if (e.target.name === 'orientacion-sexoafectiva') {
      setModalTitle('Orientación sexoafectiva:');
      setModalDescription(
        'Se refiere a la atracción emocional, afectiva y sexual hacia otras personas.'
      );
    }
    setModal(true);
  };

  const closeModal = () => {
    setModal(false);
  };

  return (
    <Flex
      w="100%"
      direction="column"
      mt="1.875rem"
    >
      <Container
        maxWidth="25rem"
        centerContent
      >
        <TMImage
          src={'/images/logo-todo-mejora-icon.svg'}
          alt={'logo corazon todo mejora'}
          boxSize={'62px'}
        />

        <Container
          ml="1rem"
          mt="1.75rm"
          justifyContent="start"
        >
          <Text
            color="#5C186A"
            lineHeight="2.125rem"
            fontSize="1.25rem"
            fontStyle="normal"
            fontWeight="600"
          >
            Cuéntanos de ti
          </Text>
          <Text
            color="#161616"
            lineHeight="1.25rem"
            fontSize="0.875rem"
            fontStyle="normal"
            fontWeight="400"
          >
            Conocerte nos ayudará a darte una mejor atención en el futuro.
          </Text>
        </Container>

        <Container
          alignContent="start"
          ml="1rem"
          mt="3rem"
        >
          <Flex
            direction="column"
            alignItems="start"
          >
            <Text
              color="#5C186A"
              lineHeight="1.125rem"
              fontSize="0.875rem"
              fontStyle="normal"
              fontWeight="600"
            >
              Nombre / Apodo
            </Text>
            <Input
              mt="0.5rem"
              borderColor="8E8E8E"
              w="19rem"
              h="1.9375rem"
              opacity="0.8"
              placeholder=""
              value={name}
              onChange={(event) => {
                setName(event.target.value);
              }}
            />
          </Flex>
        </Container>

        <Container
          alignContent="start"
          ml="1rem"
          mt="2.375rem"
        >
          <Flex
            direction="row"
            alignItems="start"
          >
            <Flex
              direction="column"
              alignItems="start"
            >
              <Text
                color="#5C186A"
                lineHeight="1.125rem"
                fontSize="0.875rem"
                fontStyle="normal"
                fontWeight="600"
              >
                Edad
              </Text>
              <Input
                mt="0.5rem"
                borderColor="8E8E8E"
                w="6.0625rem"
                h="1.9375rem"
                opacity="0.8"
                placeholder=""
                value={age}
                onChange={(event) => {
                  setAge(event.target.value);
                }}
              />
            </Flex>

            <Flex
              direction="column"
              alignItems="start"
              ml="0.375rem"
            >
              <Text
                color="#5C186A"
                lineHeight="1.125rem"
                fontSize="0.875rem"
                fontStyle="normal"
                fontWeight="600"
              >
                Comuna
              </Text>
              <Input
                mt="0.5rem"
                borderColor="8E8E8E"
                w="12.5625rem"
                h="1.9375rem"
                opacity="0.8"
                placeholder=""
                value={comuna}
                onChange={(event) => {
                  setComuna(event.target.value);
                }}
              />
            </Flex>
          </Flex>
        </Container>

        <Container
          mt="2.5rem"
          ml="1rem"
        >
          <Flex
            direction="row"
            alignItems="center"
          >
            <Text
              color="#5C186A"
              lineHeight="1.125rem"
              fontSize="0.875rem"
              fontStyle="normal"
              fontWeight="600"
              mr="0.5rem"
            >
              ¿Cuál es tu identidad de género?
            </Text>
            <Button
              size="xs"
              backgroundColor="white"
              onClick={showModal}
            >
              <Image
                src="/icons/info.svg"
                alt="Ícono de información"
                w="0.875rem"
                name="identidad-genero"
              />
            </Button>
          </Flex>
          <ButtonGroup mt="0.625rem">
            <Flex
              direction="column"
              alignItems="start"
              gap="0.4rem"
            >
              <Flex
                direction="row"
                width="19rem"
                gap="0.4rem"
              >
                <Button
                  {...clickedProps(gender, 'mujer')}
                  onClick={() => {
                    setGender('mujer');
                  }}
                  w="6.104rem"
                >
                  Mujer
                </Button>
                <Button
                  {...clickedProps(gender, 'nb')}
                  onClick={() => {
                    setGender('nb');
                  }}
                >
                  No Binarie
                </Button>
                <Button
                  width="6.104rem"
                  {...clickedProps(gender, 'hombre')}
                  onClick={() => {
                    setGender('hombre');
                  }}
                >
                  Hombre
                </Button>
              </Flex>
              <Flex
                align="stretch"
                width="19rem"
                gap="0.4rem"
              >
                <Button
                  {...clickedProps(gender, 'descubriendolo')}
                  onClick={() => {
                    setGender('descubriendolo');
                  }}
                >
                  Estoy descubriéndolo
                </Button>
                <Button
                  {...clickedProps(gender, 'no responde')}
                  onClick={() => {
                    setGender('no responde');
                  }}
                >
                  No quiero responder
                </Button>
              </Flex>
            </Flex>
          </ButtonGroup>
        </Container>

        <Container
          mt="2.5rem"
          ml="1rem"
        >
          <Flex
            direction="row"
            alignItems="center"
          >
            <Text
              color="#5C186A"
              lineHeight="1.125rem"
              fontSize="0.875rem"
              fontStyle="normal"
              fontWeight="600"
            >
              ¿Cuál es tu sexo biológico?
            </Text>
            <Button
              size="xs"
              backgroundColor="white"
              onClick={showModal}
            >
              <Image
                src="/icons/info.svg"
                alt="Ícono de información"
                w="0.875rem"
                name="sexo-biologico"
              />
            </Button>
          </Flex>
          <ButtonGroup mt="0.625rem">
            <Flex
              direction="column"
              alignItems="start"
              gap="0.4rem"
            >
              <Flex
                direction="row"
                width="19rem"
                gap="0.4rem"
              >
                <Button
                  w="6.104rem"
                  {...clickedProps(bioSex, 'hembra')}
                  onClick={() => {
                    setBioSex('hembra');
                  }}
                >
                  Hembra
                </Button>
                <Button
                  {...clickedProps(bioSex, 'intersexual')}
                  onClick={() => {
                    setBioSex('intersexual');
                  }}
                >
                  Intersexual
                </Button>
                <Button
                  width="6.104rem"
                  {...clickedProps(bioSex, 'macho')}
                  onClick={() => {
                    setBioSex('macho');
                  }}
                >
                  Macho
                </Button>
              </Flex>
              <Flex
                align="stretch"
                width="19rem"
                gap="0.4rem"
              >
                <Button
                  {...clickedProps(bioSex, 'no responde')}
                  onClick={() => {
                    setBioSex('no responde');
                  }}
                >
                  No quiero responder
                </Button>
              </Flex>
            </Flex>
          </ButtonGroup>
        </Container>

        <Container
          mt="2.5rem"
          ml="1rem"
        >
          <Flex
            direction="row"
            alignItems="center"
          >
            <Text
              color="#5C186A"
              lineHeight="1.125rem"
              fontSize="0.875rem"
              fontStyle="normal"
              fontWeight="600"
            >
              ¿Cuál es tu orientación sexoafectiva?
            </Text>
            <Button
              size="xs"
              backgroundColor="white"
              onClick={showModal}
            >
              <Image
                src="/icons/info.svg"
                alt="Ícono de información"
                w="0.875rem"
                name="orientacion-sexoafectiva"
              />
            </Button>
          </Flex>
          <ButtonGroup mt="0.625rem">
            <Flex
              direction="column"
              alignItems="start"
              gap="0.4rem"
            >
              <Flex
                direction="row"
                width="19rem"
                gap="0.4rem"
              >
                <Button
                  w="6.104rem"
                  {...clickedProps(sexualOrientation, 'hetero')}
                  onClick={() => {
                    setSexualOrientation('hetero');
                  }}
                >
                  Hétero
                </Button>
                <Button
                  {...clickedProps(sexualOrientation, 'bi/pan')}
                  onClick={() => {
                    setSexualOrientation('bi/pan');
                  }}
                >
                  Bi / Pan
                </Button>
                <Button
                  width="7.5625rem"
                  {...clickedProps(sexualOrientation, 'lesbiana/gay')}
                  onClick={() => {
                    setSexualOrientation('lesbiana/gay');
                  }}
                >
                  Lesbiana / Gay
                </Button>
              </Flex>
              <Flex
                align="stretch"
                width="19rem"
                gap="0.4rem"
              >
                <Button
                  {...clickedProps(sexualOrientation, 'descubriendolo')}
                  onClick={() => {
                    setSexualOrientation('descubriendolo');
                  }}
                >
                  Estoy descubriéndolo
                </Button>
                <Button
                  {...clickedProps(sexualOrientation, 'no responde')}
                  onClick={() => {
                    setSexualOrientation('no responde');
                  }}
                >
                  No quiero responder
                </Button>
              </Flex>
            </Flex>
          </ButtonGroup>
        </Container>

        <Container
          mt="2.5rem"
          ml="1rem"
        >
          <Flex
            direction="column"
            width="19rem"
          >
            <Checkbox
              spacing="0.5625rem"
              lineHeight="1.125rem"
              fontSize="0.875rem"
              fontStyle="normal"
              fontWeight="400"
              color="#161616"
            >
              <Text
                color="#161616"
                lineHeight="1.25rem"
                fontSize="0.875rem"
                fontStyle="normal"
                fontWeight="400"
              >
                Soy una persona con discapacidad
              </Text>
            </Checkbox>
            <Checkbox
              spacing="0.5625rem"
              mt="1.25rem"
              onChange={(event) => {
                setAcceptedTerms(event.target.checked);
              }}
            >
              <Text
                color="#161616"
                lineHeight="1.25rem"
                fontSize="0.875rem"
                fontStyle="normal"
                fontWeight="400"
              >
                Acepto los
                <TMLinkButton href="https://www.google.com">
                  términos y condiciones <span>*</span>
                </TMLinkButton>
              </Text>
            </Checkbox>
          </Flex>
        </Container>

        <Container
          justifyContent="start"
          mt="1.5rem"
          ml="1rem"
          mb="2.65rem"
        >
          <TMButton
            bgColor="#5C186A"
            textColor="#FFFFFF"
            w="19rem"
            h="2.875rem"
            isDisabled={!validateFields()}
            onClick={handleClick}
          >
            Comenzar a chatear
          </TMButton>
        </Container>
      </Container>

      {openModal ? (
        <Modal
          isOpen={openModal}
          onClose={closeModal}
        >
          <ModalOverlay />
          <ModalContent>
            <ModalCloseButton />
            <ModalBody>
              <Text
                color="#5C186A"
                lineHeight="1.125rem"
                fontSize="0.875rem"
                fontStyle="normal"
                fontWeight="700"
                mt="1.5rem"
                ml="1rem"
                mb="0.875rem"
              >
                {modalTitle}
              </Text>
              <Text
                color="#161616"
                lineHeight="1.25rem"
                fontSize="0.875rem"
                fontStyle="normal"
                fontWeight="400"
                ml="1rem"
                mb="2.65rem"
              >
                {modalDescription}
              </Text>
            </ModalBody>
          </ModalContent>
        </Modal>
      ) : null}
    </Flex>
  );
};

export default UsuarieForm;
